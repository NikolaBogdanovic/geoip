GeoIP Locator API

Requires any MaxMind GeoIP (paid) or GeoLite (free) legacy database (not the new GeoIP2 or GeoLite2), in binary DAT format (not textual CSV): http://www.maxmind.com/en/geolocation_landing

Wiki: https://bitbucket.org/NikolaBogdanovic/geoip/wiki/

Help: http://NikolaBogdanovic.bitbucket.org/GeoIP/