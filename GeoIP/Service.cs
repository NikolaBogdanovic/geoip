namespace GeoIP
{
    using System;
    using System.ComponentModel;
    using System.Diagnostics.Contracts;
    using System.Globalization;
    using System.IO;
    using System.Net;
    using System.Net.Sockets;
    using System.Runtime.Caching;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// Constants representing network speeds.
    /// </summary>
    public enum NetSpeed
    {
        /// <summary>
        /// Unknown
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Dial-up
        /// </summary>
        DialUp = 1,

        /// <summary>
        /// Cable or DSL
        /// </summary>
        CableOrDSL = 2,

        /// <summary>
        /// Corporate (optical)
        /// </summary>
        Corporate = 3,

        /// <summary>
        /// Cellular (mobile)
        /// </summary>
        Cellular = 4
    }

    /// <summary>
    /// GeoIP service.
    /// </summary>
    public sealed class Service : IDisposable
    {
        private const int CountryBegin = 16776960;
        private const int StateBeginRev0 = 16700000;
        private const int StateBeginRev1 = 16000000;
        private const int StructureInfoMaxSize = 20;
        private const int DatabaseInfoMaxSize = 100;
        private const int FullRecordLength = 100;
        private const int SegmentRecordLength = 3;
        private const int StandardRecordLength = 3;
        private const int OrganizationRecordLength = 4;
        private const int MaxRecordLength = 4;
        private const int MaxOrganizationRecordLength = 1000;
        private const int FipsRange = 360;
        private const int UsOffset = 1;
        private const int CanadaOffset = 677;
        private const int WorldOffset = 1353;

        private static readonly SemaphoreSlim Lock = new SemaphoreSlim(1, 1);

        private readonly object _lock = new object();
        private readonly int _recordLength = StandardRecordLength;
        private readonly int _databaseSegments;
        private readonly DatabaseEdition _db;

        private FileStream _file;

        /// <summary>
        /// Gets the GeoIP database.
        /// </summary>
        public Database Database { get; private set; }

        private void GetDatabase()
        {
            bool hasStructureInfo = false;
            _file.Seek(-3, SeekOrigin.End);
            var sep = new byte[3];
            for(int i = 0; i < StructureInfoMaxSize; i++)
            {
                _file.Read(sep, 0, 3);
                if(sep[0] == 255 && sep[1] == 255 && sep[2] == 255)
                {
                    hasStructureInfo = true;
                    break;
                }
                _file.Seek(-4, SeekOrigin.Current);
            }
            if(hasStructureInfo)
                _file.Seek(-6, SeekOrigin.Current);
            else
                _file.Seek(-3, SeekOrigin.End);
            for(int i = 0; i < DatabaseInfoMaxSize; i++)
            {
                _file.Read(sep, 0, 3);
                if(sep[0] == 0 && sep[1] == 0 && sep[2] == 0)
                {
                    var info = new byte[i];
                    var info2 = new char[i];
                    _file.Read(info, 0, i);
                    for(int j = 0; j < i; j++)
                        info2[j] = (char)info[j];
                    Database = new Database(new string(info2));
                    break;
                }
                _file.Seek(-4, SeekOrigin.Current);
            }
        }

        private Service(DatabaseEdition db, FileStream file)
        {
            _db = db;
            _file = file;
            GetDatabase();
            _file.Seek(-3, SeekOrigin.End);
            for(int i = 0; i < StructureInfoMaxSize; i++)
            {
                var sep = new byte[3];
                _file.Read(sep, 0, 3);
                if(sep[0] == 255 && sep[1] == 255 && sep[2] == 255)
                {
                    int e = _file.ReadByte();
                    if(e > 105)
                        e -= 105;
                    _db = (DatabaseEdition)e;
                    if(_db.GetName() != db.GetName())
                        throw new ArgumentOutOfRangeException("db", db.GetName(), @"Argument Out Of Range Exception");
                    switch(_db)
                    {
                        case DatabaseEdition.ASNum:
                        case DatabaseEdition.ASNumV6:
                        case DatabaseEdition.CityRev0:
                        case DatabaseEdition.CityV6Rev0:
                        case DatabaseEdition.CityRev1:
                        case DatabaseEdition.CityV6Rev1:
                        case DatabaseEdition.ISP:
                        case DatabaseEdition.ISPV6:
                        case DatabaseEdition.NetSpeedRev1:
                        case DatabaseEdition.NetSpeedV6Rev1:
                        case DatabaseEdition.Org:
                        case DatabaseEdition.OrgV6:
                        {
                            _databaseSegments = 0;
                            if(_db == DatabaseEdition.ASNum || _db == DatabaseEdition.ASNumV6 || _db == DatabaseEdition.CityRev0 || _db == DatabaseEdition.CityV6Rev0 || _db == DatabaseEdition.CityRev1 || _db == DatabaseEdition.CityV6Rev1 || _db == DatabaseEdition.NetSpeedRev1 || _db == DatabaseEdition.NetSpeedV6Rev1)
                                _recordLength = StandardRecordLength;
                            else
                                _recordLength = OrganizationRecordLength;
                            var buf = new byte[SegmentRecordLength];
                            _file.Read(buf, 0, SegmentRecordLength);
                            for(int j = 0; j < SegmentRecordLength; j++)
                                _databaseSegments += buf[j] << (j * 8);
                            break;
                        }
                        case DatabaseEdition.RegionRev0:
                        {
                            _databaseSegments = StateBeginRev0;
                            _recordLength = StandardRecordLength;
                            break;
                        }
                        case DatabaseEdition.RegionRev1:
                        {
                            _databaseSegments = StateBeginRev1;
                            _recordLength = StandardRecordLength;
                            break;
                        }
                    }
                    break;
                }
                _file.Seek(-4, SeekOrigin.Current);
            }
            if(_db == DatabaseEdition.Country || _db == DatabaseEdition.CountryV6 || _db == DatabaseEdition.NetSpeedRev0 || _db == DatabaseEdition.Proxy)
            {
                _databaseSegments = CountryBegin;
                _recordLength = StandardRecordLength;
            }
        }

        /// <inheritdoc/>
        public void Dispose()
        {
            if(_file != null)
            {
                MemoryCache.Default.Remove(_db.GetName() + (_file.IsAsync ? " Async" : null));
                _file.Dispose();
                _file = null;
            }
        }

        private static void RemovedCallback(CacheEntryRemovedArguments args)
        {
            if(args.RemovedReason != CacheEntryRemovedReason.Removed && args.CacheItem != null)
            {
                var geo = args.CacheItem.Value as Service;
                if(geo != null)
                    geo.Dispose();
            }
        }

        /// <summary>
        /// Creates a GeoIP service.
        /// </summary>
        /// <param name="db">The database edition.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// A GeoIP service.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static Service Create(DatabaseEdition db, CancellationToken cancel)
        {
            string key = db.GetName();
            MemoryCache cache = MemoryCache.Default;
            var geo = cache[key] as Service;
            if(geo == null)
            {
                Lock.Wait(cancel);
                try
                {
                    geo = cache.Get(key) as Service;
                    if(geo == null)
                    {
                        string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Properties.Settings.Default.DataPath, "GeoIP" + db.GetName() + ".dat");
                        geo = new Service(db, Databases.GetFile(path, cancel));
                        var cip = new CacheItemPolicy
                        {
                            Priority = CacheItemPriority.NotRemovable,
                            RemovedCallback = RemovedCallback,
                            SlidingExpiration = TimeSpan.FromMinutes(Properties.Settings.Default.SlidingExpiration)
                        };
                        cip.ChangeMonitors.Add(new HostFileChangeMonitor(new[] { path }));
                        cache.Add(key, geo, cip);
                    }
                }
                finally
                {
                    Lock.Release();
                }
            }
            return geo;
        }

        /// <summary>
        /// Creates asynchronously a GeoIP service.
        /// </summary>
        /// <param name="db">The database edition.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// A GeoIP service.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static async Task<Service> CreateAsync(DatabaseEdition db, CancellationToken cancel)
        {
            string key = db.GetName() + " Async";
            MemoryCache cache = MemoryCache.Default;
            var geo = cache[key] as Service;
            if(geo == null)
            {
                await Lock.WaitAsync(cancel).ConfigureAwait(false);
                try
                {
                    geo = cache.Get(key) as Service;
                    if(geo == null)
                    {
                        string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Properties.Settings.Default.DataPath, "GeoIP" + db.GetName() + ".dat");
                        geo = new Service(db, await Databases.GetFileAsync(path, cancel).ConfigureAwait(false));
                        var cip = new CacheItemPolicy
                        {
                            Priority = CacheItemPriority.NotRemovable,
                            RemovedCallback = RemovedCallback,
                            SlidingExpiration = TimeSpan.FromMinutes(Properties.Settings.Default.SlidingExpiration)
                        };
                        cip.ChangeMonitors.Add(new HostFileChangeMonitor(new[] { path }));
                        cache.Add(key, geo, cip);
                    }
                }
                finally
                {
                    Lock.Release();
                }
            }
            return geo;
        }

        private int SeekCountryV4(IPAddress ip4)
        {
            var buf = new byte[2 * MaxRecordLength];
            var x = new int[2];
            int offset = 0;
            lock(_lock)
            {
                for(int depth = 31; depth >= 0; depth--)
                {
                    _file.Seek(2 * _recordLength * offset, SeekOrigin.Begin);
                    _file.Read(buf, 0, 2 * MaxRecordLength);
                    for(int i = 0; i < 2; i++)
                    {
                        x[i] = 0;
                        for(int j = 0; j < _recordLength; j++)
                        {
                            int k = buf[(i * _recordLength) + j];
                            if(k < 0)
                                k += 256;
                            x[i] += k << (j * 8);
                        }
                    }
                    long num = 0;
                    byte[] address = ip4.GetAddressBytes();
                    for(int i = 0; i < 4; i++)
                    {
                        long j = address[i];
                        if(j < 0)
                            j += 256;
                        num += j << ((3 - i) * 8);
                    }
                    if((num & (1 << depth)) > 0)
                    {
                        if(x[1] >= _databaseSegments)
                            return x[1];
                        offset = x[1];
                    }
                    else
                    {
                        if(x[0] >= _databaseSegments)
                            return x[0];
                        offset = x[0];
                    }
                }
            }
            return 0;
        }

        private int SeekCountryV6(IPAddress ip6)
        {
            var buf = new byte[2 * MaxRecordLength];
            var x = new int[2];
            int offset = 0;
            lock(_lock)
            {
                for(int depth = 127; depth >= 0; depth--)
                {
                    _file.Seek(2 * _recordLength * offset, SeekOrigin.Begin);
                    _file.Read(buf, 0, 2 * MaxRecordLength);
                    for(int i = 0; i < 2; i++)
                    {
                        x[i] = 0;
                        for(int j = 0; j < _recordLength; j++)
                        {
                            int k = buf[(i * _recordLength) + j];
                            if(k < 0)
                                k += 256;
                            x[i] += k << (j * 8);
                        }
                    }
                    int num = 127 - depth;
                    int idx = num >> 3;
                    int mask = 1 << num & 7 ^ 7;
                    if((ip6.GetAddressBytes()[idx] & mask) > 0)
                    {
                        if(x[1] >= _databaseSegments)
                            return x[1];
                        offset = x[1];
                    }
                    else
                    {
                        if(x[0] >= _databaseSegments)
                            return x[0];
                        offset = x[0];
                    }
                }
            }
            return 0;
        }

        private Country SeekCountryV4(IPAddress ip4, CancellationToken cancel)
        {
            return Country.Create(SeekCountryV4(ip4) - CountryBegin, cancel);
        }

        private Country SeekCountryV6(IPAddress ip6, CancellationToken cancel)
        {
            return Country.Create(SeekCountryV6(ip6) - CountryBegin, cancel);
        }

        /// <summary>
        /// Gets the GeoIP country.
        /// </summary>
        /// <param name="ip">The IPv4 or IPv6 address.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// The GeoIP country.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static Country GetCountry(IPAddress ip, CancellationToken cancel)
        {
            Contract.Requires(ip != null);
            Contract.Requires(ip.AddressFamily == AddressFamily.InterNetwork || ip.AddressFamily == AddressFamily.InterNetworkV6);

            Service geo = null;
            try
            {
                if(ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    geo = Create(DatabaseEdition.Country, cancel);
                    return geo.SeekCountryV4(ip, cancel);
                }
                geo = Create(DatabaseEdition.CountryV6, cancel);
                return geo.SeekCountryV6(ip, cancel);
            }
            catch
            {
                if(geo != null)
                    geo.Dispose();
                throw;
            }
        }

        private async Task<Country> SeekCountryV4Async(IPAddress ip4, CancellationToken cancel)
        {
            return await Country.CreateAsync(SeekCountryV4(ip4) - CountryBegin, cancel).ConfigureAwait(false);
        }

        private async Task<Country> SeekCountryV6Async(IPAddress ip6, CancellationToken cancel)
        {
            return await Country.CreateAsync(SeekCountryV6(ip6) - CountryBegin, cancel).ConfigureAwait(false);
        }

        /// <summary>
        /// Gets asynchronously the GeoIP country.
        /// </summary>
        /// <param name="ip">The IPv4 or IPv6 address.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// The GeoIP country.
        /// </returns>
        public static async Task<Country> GetCountryAsync(IPAddress ip, CancellationToken cancel)
        {
            Contract.Requires(ip != null);
            Contract.Requires(ip.AddressFamily == AddressFamily.InterNetwork || ip.AddressFamily == AddressFamily.InterNetworkV6);

            Service geo = null;
            try
            {
                if(ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    geo = await CreateAsync(DatabaseEdition.Country, cancel).ConfigureAwait(false);
                    return await geo.SeekCountryV4Async(ip, cancel).ConfigureAwait(false);
                }
                geo = await CreateAsync(DatabaseEdition.CountryV6, cancel).ConfigureAwait(false);
                return await geo.SeekCountryV6Async(ip, cancel).ConfigureAwait(false);
            }
            catch
            {
                if(geo != null)
                    geo.Dispose();
                throw;
            }
        }

        private Region SeekRegionV4(IPAddress ip4, CancellationToken cancel)
        {
            int seekRegion;
            var ch = new char[2];
            if(_db == DatabaseEdition.RegionRev0)
            {
                seekRegion = SeekCountryV4(ip4) - StateBeginRev0;
                if(seekRegion < 1000)
                    return Region.Create(seekRegion, "00", cancel);
                ch[0] = (char)(((seekRegion - 1000) / 26) + 65);
                ch[1] = (char)(((seekRegion - 1000) % 26) + 65);
                return Region.Create(225, new string(ch), cancel);
            }
            if(_db == DatabaseEdition.RegionRev1)
            {
                seekRegion = SeekCountryV4(ip4) - StateBeginRev1;
                if(seekRegion >= UsOffset)
                {
                    if(seekRegion < CanadaOffset)
                    {
                        ch[0] = (char)(((seekRegion - UsOffset) / 26) + 65);
                        ch[1] = (char)(((seekRegion - UsOffset) % 26) + 65);
                        return Region.Create(225, new string(ch), cancel);
                    }
                    if(seekRegion < WorldOffset)
                    {
                        ch[0] = (char)(((seekRegion - CanadaOffset) / 26) + 65);
                        ch[1] = (char)(((seekRegion - CanadaOffset) % 26) + 65);
                        return Region.Create(38, new string(ch), cancel);
                    }
                    return Region.Create((seekRegion - WorldOffset) / FipsRange, "00", cancel);
                }
            }
            return null;
        }

        /// <summary>
        /// Gets the GeoIP region.
        /// </summary>
        /// <param name="ip4">The IPv4 address.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// The GeoIP region.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static Region GetRegionV4(IPAddress ip4, CancellationToken cancel)
        {
            Contract.Requires(ip4 != null);
            Contract.Requires(ip4.AddressFamily == AddressFamily.InterNetwork);

            Service geo = null;
            try
            {
                geo = Create(DatabaseEdition.RegionRev0, cancel);
                return geo.SeekRegionV4(ip4, cancel);
            }
            catch
            {
                if(geo != null)
                    geo.Dispose();
                throw;
            }
        }

        private async Task<Region> SeekRegionV4Async(IPAddress ip4, CancellationToken cancel)
        {
            int seekRegion;
            var ch = new char[2];
            if(_db == DatabaseEdition.RegionRev0)
            {
                seekRegion = SeekCountryV4(ip4) - StateBeginRev0;
                if(seekRegion < 1000)
                    return await Region.CreateAsync(seekRegion, "00", cancel).ConfigureAwait(false);
                ch[0] = (char)(((seekRegion - 1000) / 26) + 65);
                ch[1] = (char)(((seekRegion - 1000) % 26) + 65);
                return await Region.CreateAsync(225, new string(ch), cancel).ConfigureAwait(false);
            }
            if(_db == DatabaseEdition.RegionRev1)
            {
                seekRegion = SeekCountryV4(ip4) - StateBeginRev1;
                if(seekRegion >= UsOffset)
                {
                    if(seekRegion < CanadaOffset)
                    {
                        ch[0] = (char)(((seekRegion - UsOffset) / 26) + 65);
                        ch[1] = (char)(((seekRegion - UsOffset) % 26) + 65);
                        return await Region.CreateAsync(225, new string(ch), cancel).ConfigureAwait(false);
                    }
                    if(seekRegion < WorldOffset)
                    {
                        ch[0] = (char)(((seekRegion - CanadaOffset) / 26) + 65);
                        ch[1] = (char)(((seekRegion - CanadaOffset) % 26) + 65);
                        return await Region.CreateAsync(38, new string(ch), cancel).ConfigureAwait(false);
                    }
                    return await Region.CreateAsync((seekRegion - WorldOffset) / FipsRange, "00", cancel).ConfigureAwait(false);
                }
            }
            return null;
        }

        /// <summary>
        /// Gets asynchronously the GeoIP region.
        /// </summary>
        /// <param name="ip4">The IPv4 address.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// The GeoIP region.
        /// </returns>
        public static async Task<Region> GetRegionV4Async(IPAddress ip4, CancellationToken cancel)
        {
            Contract.Requires(ip4 != null);
            Contract.Requires(ip4.AddressFamily == AddressFamily.InterNetwork);

            Service geo = null;
            try
            {
                geo = await CreateAsync(DatabaseEdition.RegionRev0, cancel).ConfigureAwait(false);
                return await geo.SeekRegionV4Async(ip4, cancel).ConfigureAwait(false);
            }
            catch
            {
                if(geo != null)
                    geo.Dispose();
                throw;
            }
        }

        private Location SeekLocationV4(IPAddress ip4, CancellationToken cancel)
        {
            var recordBuf = new byte[FullRecordLength];
            var recordBuf2 = new char[FullRecordLength];
            int recordBufOffset = 0;
            int strLength = 0;
            double latitude = 0, longitude = 0;
            int seekCountry = SeekCountryV4(ip4);
            if(seekCountry == _databaseSegments)
                return null;
            int recordPointer = seekCountry + (((2 * _recordLength) - 1) * _databaseSegments);
            lock(_lock)
            {
                _file.Seek(recordPointer, SeekOrigin.Begin);
                _file.Read(recordBuf, 0, FullRecordLength);
            }
            for(int i = 0; i < FullRecordLength; i++)
                recordBuf2[i] = (char)recordBuf[i];
            int country = recordBuf[0];
            recordBufOffset++;
            while(recordBuf[recordBufOffset + strLength] != char.MinValue)
                strLength++;
            var loc = Location.Create(country, strLength > 0 ? new string(recordBuf2, recordBufOffset, strLength) : "00", cancel);
            recordBufOffset += strLength + 1;
            strLength = 0;
            while(recordBuf[recordBufOffset + strLength] != char.MinValue)
                strLength++;
            if(strLength > 0)
                loc.City = new string(recordBuf2, recordBufOffset, strLength);
            recordBufOffset += strLength + 1;
            strLength = 0;
            while(recordBuf[recordBufOffset + strLength] != char.MinValue)
                strLength++;
            if(strLength > 0)
                loc.PostalCode = new string(recordBuf2, recordBufOffset, strLength);
            recordBufOffset += strLength + 1;
            for(int i = 0; i < 3; i++)
                latitude += recordBuf[recordBufOffset + i] << (i * 8);
            loc.Latitude = (latitude / 10000) - 180;
            recordBufOffset += 3;
            for(int i = 0; i < 3; i++)
                longitude += recordBuf[recordBufOffset + i] << (i * 8);
            loc.Longitude = (longitude / 10000) - 180;
            if(_db == DatabaseEdition.CityRev1)
            {
                int metroArea = 0;
                if(loc.Region.Country.Alpha2Code == "US")
                {
                    recordBufOffset += 3;
                    for(int i = 0; i < 3; i++)
                        metroArea += recordBuf[recordBufOffset + i] << (i * 8);
                    loc.MetroCode = loc.DmaCode = metroArea / 1000;
                    loc.AreaCode = metroArea % 1000;
                }
            }
            return loc;
        }

        private Location SeekLocationV6(IPAddress ip6, CancellationToken cancel)
        {
            var recordBuf = new byte[FullRecordLength];
            var recordBuf2 = new char[FullRecordLength];
            int recordBufOffset = 0;
            int strLength = 0;
            double latitude = 0, longitude = 0;
            int seekCountry = SeekCountryV6(ip6);
            if(seekCountry == _databaseSegments)
                return null;
            int recordPointer = seekCountry + (((2 * _recordLength) - 1) * _databaseSegments);
            lock(_lock)
            {
                _file.Seek(recordPointer, SeekOrigin.Begin);
                _file.Read(recordBuf, 0, FullRecordLength);
            }
            for(int i = 0; i < FullRecordLength; i++)
                recordBuf2[i] = (char)recordBuf[i];
            int country = recordBuf[0];
            recordBufOffset++;
            while(recordBuf[recordBufOffset + strLength] != char.MinValue)
                strLength++;
            var loc = Location.Create(country, strLength > 0 ? new string(recordBuf2, recordBufOffset, strLength) : "00", cancel);
            recordBufOffset += strLength + 1;
            strLength = 0;
            while(recordBuf[recordBufOffset + strLength] != char.MinValue)
                strLength++;
            if(strLength > 0)
                loc.City = new string(recordBuf2, recordBufOffset, strLength);
            recordBufOffset += strLength + 1;
            strLength = 0;
            while(recordBuf[recordBufOffset + strLength] != char.MinValue)
                strLength++;
            if(strLength > 0)
                loc.PostalCode = new string(recordBuf2, recordBufOffset, strLength);
            recordBufOffset += strLength + 1;
            for(int i = 0; i < 3; i++)
                latitude += recordBuf[recordBufOffset + i] << (i * 8);
            loc.Latitude = (latitude / 10000) - 180;
            recordBufOffset += 3;
            for(int i = 0; i < 3; i++)
                longitude += recordBuf[recordBufOffset + i] << (i * 8);
            loc.Longitude = (longitude / 10000) - 180;
            if(_db == DatabaseEdition.CityV6Rev1)
            {
                int metroArea = 0;
                if(loc.Region.Country.Alpha2Code == "US")
                {
                    recordBufOffset += 3;
                    for(int i = 0; i < 3; i++)
                        metroArea += recordBuf[recordBufOffset + i] << (i * 8);
                    loc.MetroCode = loc.DmaCode = metroArea / 1000;
                    loc.AreaCode = metroArea % 1000;
                }
            }
            return loc;
        }

        /// <summary>
        /// Gets the GeoIP location.
        /// </summary>
        /// <param name="ip">The IPv4 or IPv6 address.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// The GeoIP location.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static Location GetLocation(IPAddress ip, CancellationToken cancel)
        {
            Contract.Requires(ip != null);
            Contract.Requires(ip.AddressFamily == AddressFamily.InterNetwork || ip.AddressFamily == AddressFamily.InterNetworkV6);

            Service geo = null;
            try
            {
                if(ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    geo = Create(DatabaseEdition.CityRev0, cancel);
                    return geo.SeekLocationV4(ip, cancel);
                }
                geo = Create(DatabaseEdition.CityV6Rev0, cancel);
                return geo.SeekLocationV6(ip, cancel);
            }
            catch
            {
                if(geo != null)
                    geo.Dispose();
                throw;
            }
        }

        private async Task<Location> SeekLocationV4Async(IPAddress ip4, CancellationToken cancel)
        {
            var recordBuf = new byte[FullRecordLength];
            var recordBuf2 = new char[FullRecordLength];
            int recordBufOffset = 0;
            int strLength = 0;
            double latitude = 0, longitude = 0;
            int seekCountry = SeekCountryV4(ip4);
            if(seekCountry == _databaseSegments)
                return null;
            int recordPointer = seekCountry + (((2 * _recordLength) - 1) * _databaseSegments);
            lock(_lock)
            {
                _file.Seek(recordPointer, SeekOrigin.Begin);
                _file.Read(recordBuf, 0, FullRecordLength);
            }
            for(int i = 0; i < FullRecordLength; i++)
                recordBuf2[i] = (char)recordBuf[i];
            int country = recordBuf[0];
            recordBufOffset++;
            while(recordBuf[recordBufOffset + strLength] != char.MinValue)
                strLength++;
            var loc = await Location.CreateAsync(country, strLength > 0 ? new string(recordBuf2, recordBufOffset, strLength) : "00", cancel).ConfigureAwait(false);
            recordBufOffset += strLength + 1;
            strLength = 0;
            while(recordBuf[recordBufOffset + strLength] != char.MinValue)
                strLength++;
            if(strLength > 0)
                loc.City = new string(recordBuf2, recordBufOffset, strLength);
            recordBufOffset += strLength + 1;
            strLength = 0;
            while(recordBuf[recordBufOffset + strLength] != char.MinValue)
                strLength++;
            if(strLength > 0)
                loc.PostalCode = new string(recordBuf2, recordBufOffset, strLength);
            recordBufOffset += strLength + 1;
            for(int i = 0; i < 3; i++)
                latitude += recordBuf[recordBufOffset + i] << (i * 8);
            loc.Latitude = (latitude / 10000) - 180;
            recordBufOffset += 3;
            for(int i = 0; i < 3; i++)
                longitude += recordBuf[recordBufOffset + i] << (i * 8);
            loc.Longitude = (longitude / 10000) - 180;
            if(_db == DatabaseEdition.CityRev1)
            {
                int metroArea = 0;
                if(loc.Region.Country.Alpha2Code == "US")
                {
                    recordBufOffset += 3;
                    for(int i = 0; i < 3; i++)
                        metroArea += recordBuf[recordBufOffset + i] << (i * 8);
                    loc.MetroCode = loc.DmaCode = metroArea / 1000;
                    loc.AreaCode = metroArea % 1000;
                }
            }
            return loc;
        }

        private async Task<Location> SeekLocationV6Async(IPAddress ip6, CancellationToken cancel)
        {
            var recordBuf = new byte[FullRecordLength];
            var recordBuf2 = new char[FullRecordLength];
            int recordBufOffset = 0;
            int strLength = 0;
            double latitude = 0, longitude = 0;
            int seekCountry = SeekCountryV6(ip6);
            if(seekCountry == _databaseSegments)
                return null;
            int recordPointer = seekCountry + (((2 * _recordLength) - 1) * _databaseSegments);
            lock(_lock)
            {
                _file.Seek(recordPointer, SeekOrigin.Begin);
                _file.Read(recordBuf, 0, FullRecordLength);
            }
            for(int i = 0; i < FullRecordLength; i++)
                recordBuf2[i] = (char)recordBuf[i];
            int country = recordBuf[0];
            recordBufOffset++;
            while(recordBuf[recordBufOffset + strLength] != char.MinValue)
                strLength++;
            var loc = await Location.CreateAsync(country, strLength > 0 ? new string(recordBuf2, recordBufOffset, strLength) : "00", cancel).ConfigureAwait(false);
            recordBufOffset += strLength + 1;
            strLength = 0;
            while(recordBuf[recordBufOffset + strLength] != char.MinValue)
                strLength++;
            if(strLength > 0)
                loc.City = new string(recordBuf2, recordBufOffset, strLength);
            recordBufOffset += strLength + 1;
            strLength = 0;
            while(recordBuf[recordBufOffset + strLength] != char.MinValue)
                strLength++;
            if(strLength > 0)
                loc.PostalCode = new string(recordBuf2, recordBufOffset, strLength);
            recordBufOffset += strLength + 1;
            for(int i = 0; i < 3; i++)
                latitude += recordBuf[recordBufOffset + i] << (i * 8);
            loc.Latitude = (latitude / 10000) - 180;
            recordBufOffset += 3;
            for(int i = 0; i < 3; i++)
                longitude += recordBuf[recordBufOffset + i] << (i * 8);
            loc.Longitude = (longitude / 10000) - 180;
            if(_db == DatabaseEdition.CityV6Rev1)
            {
                int metroArea = 0;
                if(loc.Region.Country.Alpha2Code == "US")
                {
                    recordBufOffset += 3;
                    for(int i = 0; i < 3; i++)
                        metroArea += recordBuf[recordBufOffset + i] << (i * 8);
                    loc.MetroCode = loc.DmaCode = metroArea / 1000;
                    loc.AreaCode = metroArea % 1000;
                }
            }
            return loc;
        }

        /// <summary>
        /// Gets asynchronously the GeoIP location.
        /// </summary>
        /// <param name="ip">The IPv4 or IPv6 address.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// The GeoIP location.
        /// </returns>
        public static async Task<Location> GetLocationAsync(IPAddress ip, CancellationToken cancel)
        {
            Contract.Requires(ip != null);
            Contract.Requires(ip.AddressFamily == AddressFamily.InterNetwork || ip.AddressFamily == AddressFamily.InterNetworkV6);

            Service geo = null;
            try
            {
                if(ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    geo = await CreateAsync(DatabaseEdition.CityRev0, cancel).ConfigureAwait(false);
                    return await geo.SeekLocationV4Async(ip, cancel).ConfigureAwait(false);
                }
                geo = await CreateAsync(DatabaseEdition.CityV6Rev0, cancel).ConfigureAwait(false);
                return await geo.SeekLocationV6Async(ip, cancel).ConfigureAwait(false);
            }
            catch
            {
                if(geo != null)
                    geo.Dispose();
                throw;
            }
        }

        private NetSpeed SeekNetSpeedV4(IPAddress ip4)
        {
            return (NetSpeed)(SeekCountryV4(ip4) - _databaseSegments);
        }

        /// <summary>
        /// Gets the GeoIP network speed.
        /// </summary>
        /// <param name="ip4">The IPv4 address.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// The GeoIP network speed.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static NetSpeed GetNetSpeedV4(IPAddress ip4, CancellationToken cancel)
        {
            Contract.Requires(ip4 != null);
            Contract.Requires(ip4.AddressFamily == AddressFamily.InterNetwork);

            Service geo = null;
            try
            {
                geo = Create(DatabaseEdition.NetSpeedRev0, cancel);
                return geo.SeekNetSpeedV4(ip4);
            }
            catch
            {
                if(geo != null)
                    geo.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Gets asynchronously the GeoIP network speed.
        /// </summary>
        /// <param name="ip4">The IPv4 address.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// The GeoIP network speed.
        /// </returns>
        public static async Task<NetSpeed> GetNetSpeedV4Async(IPAddress ip4, CancellationToken cancel)
        {
            Contract.Requires(ip4 != null);
            Contract.Requires(ip4.AddressFamily == AddressFamily.InterNetwork);

            Service geo = null;
            try
            {
                geo = await CreateAsync(DatabaseEdition.NetSpeedRev0, cancel).ConfigureAwait(false);
                return geo.SeekNetSpeedV4(ip4);
            }
            catch
            {
                if(geo != null)
                    geo.Dispose();
                throw;
            }
        }

        private string SeekOrganizationV4(IPAddress ip4)
        {
            int strLength = 0;
            var recordBuf = new byte[MaxOrganizationRecordLength];
            var recordBuf2 = new char[MaxOrganizationRecordLength];
            int seekOrganization = SeekCountryV4(ip4);
            if(seekOrganization == _databaseSegments)
                return null;
            int recordPointer = seekOrganization + (((2 * _recordLength) - 1) * _databaseSegments);
            lock(_lock)
            {
                _file.Seek(recordPointer, SeekOrigin.Begin);
                _file.Read(recordBuf, 0, MaxOrganizationRecordLength);
            }
            while(recordBuf[strLength] != 0)
            {
                recordBuf2[strLength] = (char)recordBuf[strLength];
                strLength++;
            }
            recordBuf2[strLength] = char.MinValue;
            return new string(recordBuf2, 0, strLength);
        }

        private string SeekOrganizationV6(IPAddress ip6)
        {
            int strLength = 0;
            var recordBuf = new byte[MaxOrganizationRecordLength];
            var recordBuf2 = new char[MaxOrganizationRecordLength];
            int seekOrganization = SeekCountryV6(ip6);
            if(seekOrganization == _databaseSegments)
                return null;
            int recordPointer = seekOrganization + (((2 * _recordLength) - 1) * _databaseSegments);
            lock(_lock)
            {
                _file.Seek(recordPointer, SeekOrigin.Begin);
                _file.Read(recordBuf, 0, MaxOrganizationRecordLength);
            }
            while(recordBuf[strLength] != 0)
            {
                recordBuf2[strLength] = (char)recordBuf[strLength];
                strLength++;
            }
            recordBuf2[strLength] = char.MinValue;
            return new string(recordBuf2, 0, strLength);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        private static string GetGeoIP(IPAddress ip, DatabaseEdition db4, DatabaseEdition db6, CancellationToken cancel)
        {
            if(db4 != DatabaseEdition.ASNum && db4 != DatabaseEdition.Domain && db4 != DatabaseEdition.ISP && db4 != DatabaseEdition.Org)
                throw new InvalidEnumArgumentException("db4", (int)db4, typeof(DatabaseEdition));
            if(db6 != DatabaseEdition.ASNumV6 && db6 != DatabaseEdition.DomainV6 && db6 != DatabaseEdition.ISPV6 && db6 != DatabaseEdition.OrgV6)
                throw new InvalidEnumArgumentException("db6", (int)db6, typeof(DatabaseEdition));
            Service geo = null;
            try
            {
                if(ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    geo = Create(db4, cancel);
                    return geo.SeekOrganizationV4(ip);
                }
                if(ip.AddressFamily == AddressFamily.InterNetworkV6)
                {
                    geo = Create(db6, cancel);
                    return geo.SeekOrganizationV6(ip);
                }
                throw new InvalidEnumArgumentException("ip", (int)ip.AddressFamily, typeof(AddressFamily));
            }
            catch
            {
                if(geo != null)
                    geo.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Gets the GeoIP organization name.
        /// </summary>
        /// <param name="ip">The IPv4 or IPv6 address.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// The GeoIP organization name.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static string GetOrganization(IPAddress ip, CancellationToken cancel)
        {
            Contract.Requires(ip != null);
            Contract.Requires(ip.AddressFamily == AddressFamily.InterNetwork || ip.AddressFamily == AddressFamily.InterNetworkV6);

            return GetGeoIP(ip, DatabaseEdition.Org, DatabaseEdition.OrgV6, cancel);
        }

        /// <summary>
        /// Gets the GeoIP internet service provider name.
        /// </summary>
        /// <param name="ip">The IPv4 or IPv6 address.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// The GeoIP internet service provider name.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static string GetIsp(IPAddress ip, CancellationToken cancel)
        {
            Contract.Requires(ip != null);
            Contract.Requires(ip.AddressFamily == AddressFamily.InterNetwork || ip.AddressFamily == AddressFamily.InterNetworkV6);

            return GetGeoIP(ip, DatabaseEdition.ISP, DatabaseEdition.ISPV6, cancel);
        }

        /// <summary>
        /// Gets the GeoIP domain name.
        /// </summary>
        /// <param name="ip">The IPv4 or IPv6 address.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// The GeoIP domain name.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static string GetDomain(IPAddress ip, CancellationToken cancel)
        {
            Contract.Requires(ip != null);
            Contract.Requires(ip.AddressFamily == AddressFamily.InterNetwork || ip.AddressFamily == AddressFamily.InterNetworkV6);

            return GetGeoIP(ip, DatabaseEdition.Domain, DatabaseEdition.DomainV6, cancel);
        }

        /// <summary>
        /// Gets the GeoIP autonomous system number.
        /// </summary>
        /// <param name="ip">The IPv4 or IPv6 address.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// The GeoIP autonomous system number.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static int GetAsn(IPAddress ip, CancellationToken cancel)
        {
            Contract.Requires(ip != null);
            Contract.Requires(ip.AddressFamily == AddressFamily.InterNetwork || ip.AddressFamily == AddressFamily.InterNetworkV6);

            return int.Parse(GetGeoIP(ip, DatabaseEdition.ASNum, DatabaseEdition.ASNumV6, cancel), CultureInfo.InvariantCulture);
        }

        private static async Task<string> GetGeoIPAsync(IPAddress ip, DatabaseEdition db4, DatabaseEdition db6, CancellationToken cancel)
        {
            if(db4 != DatabaseEdition.ASNum && db4 != DatabaseEdition.Domain && db4 != DatabaseEdition.ISP && db4 != DatabaseEdition.Org)
                throw new InvalidEnumArgumentException("db4", (int)db4, typeof(DatabaseEdition));
            if(db6 != DatabaseEdition.ASNumV6 && db6 != DatabaseEdition.DomainV6 && db6 != DatabaseEdition.ISPV6 && db6 != DatabaseEdition.OrgV6)
                throw new InvalidEnumArgumentException("db6", (int)db6, typeof(DatabaseEdition));
            Service geo = null;
            try
            {
                if(ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    geo = await CreateAsync(db4, cancel).ConfigureAwait(false);
                    return geo.SeekOrganizationV4(ip);
                }
                if(ip.AddressFamily == AddressFamily.InterNetworkV6)
                {
                    geo = await CreateAsync(db6, cancel).ConfigureAwait(false);
                    return geo.SeekOrganizationV6(ip);
                }
                throw new InvalidEnumArgumentException("ip", (int)ip.AddressFamily, typeof(AddressFamily));
            }
            catch
            {
                if(geo != null)
                    geo.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Gets asynchronously the GeoIP organization name.
        /// </summary>
        /// <param name="ip">The IPv4 or IPv6 address.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// The GeoIP organization name.
        /// </returns>
        public static async Task<string> GetOrganizationAsync(IPAddress ip, CancellationToken cancel)
        {
            Contract.Requires(ip != null);
            Contract.Requires(ip.AddressFamily == AddressFamily.InterNetwork || ip.AddressFamily == AddressFamily.InterNetworkV6);
            
            return await GetGeoIPAsync(ip, DatabaseEdition.Org, DatabaseEdition.OrgV6, cancel).ConfigureAwait(false);
        }

        /// <summary>
        /// Gets asynchronously the GeoIP internet service provider name.
        /// </summary>
        /// <param name="ip">The IPv4 or IPv6 address.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// The GeoIP internet service provider name.
        /// </returns>
        public static async Task<string> GetIspAsync(IPAddress ip, CancellationToken cancel)
        {
            Contract.Requires(ip != null);
            Contract.Requires(ip.AddressFamily == AddressFamily.InterNetwork || ip.AddressFamily == AddressFamily.InterNetworkV6);
            
            return await GetGeoIPAsync(ip, DatabaseEdition.ISP, DatabaseEdition.ISPV6, cancel).ConfigureAwait(false);
        }

        /// <summary>
        /// Gets asynchronously the GeoIP domain name.
        /// </summary>
        /// <param name="ip">The IPv4 or IPv6 address.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// The GeoIP domain name.
        /// </returns>
        public static async Task<string> GetDomainAsync(IPAddress ip, CancellationToken cancel)
        {
            Contract.Requires(ip != null);
            Contract.Requires(ip.AddressFamily == AddressFamily.InterNetwork || ip.AddressFamily == AddressFamily.InterNetworkV6);
            
            return await GetGeoIPAsync(ip, DatabaseEdition.Domain, DatabaseEdition.DomainV6, cancel).ConfigureAwait(false);
        }

        /// <summary>
        /// Gets asynchronously the GeoIP autonomous system number.
        /// </summary>
        /// <param name="ip">The IPv4 or IPv6 address.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// The GeoIP autonomous system number.
        /// </returns>
        public static async Task<int> GetAsnAsync(IPAddress ip, CancellationToken cancel)
        {
            Contract.Requires(ip != null);
            Contract.Requires(ip.AddressFamily == AddressFamily.InterNetwork || ip.AddressFamily == AddressFamily.InterNetworkV6);
            
            return int.Parse(await GetGeoIPAsync(ip, DatabaseEdition.ASNum, DatabaseEdition.ASNumV6, cancel).ConfigureAwait(false), CultureInfo.InvariantCulture);
        }
    }
}
