namespace GeoIP
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// Location information.
    /// </summary>
    public sealed class Location
    {
        /// <summary>
        /// Gets the location region.
        /// </summary>
        public Region Region { get; private set; }

        /// <summary>
        /// Gets the location latitude.
        /// </summary>
        public double Latitude { get; internal set; }

        /// <summary>
        /// Gets the location longitude.
        /// </summary>
        public double Longitude { get; internal set; }

        /// <summary>
        /// Gets the location city.
        /// </summary>
        public string City { get; internal set; }

        /// <summary>
        /// Gets the location postal code.
        /// </summary>
        public string PostalCode { get; internal set; }

        /// <summary>
        /// Gets the location Designated Market Area code.
        /// </summary>
        public int DmaCode { get; internal set; }

        /// <summary>
        /// Gets the location area code.
        /// </summary>
        public int AreaCode { get; internal set; }

        /// <summary>
        /// Gets the location metro code.
        /// </summary>
        public int MetroCode { get; internal set; }
        
        private Location()
        {
        }

        internal static Location Create(int country, string region, CancellationToken cancel)
        {
            return new Location
            {
                Region = Region.Create(country, region, cancel)
            };
        }

        internal static async Task<Location> CreateAsync(int country, string region, CancellationToken cancel)
        {
            return new Location
            {
                Region = await Region.CreateAsync(country, region, cancel).ConfigureAwait(false)
            };
        }
    }

    /// <summary>
    /// Location services.
    /// </summary>
    public static class Locations
    {
        private const double Radians = Math.PI / 180;
        private const double EarthDiameter = 12756.4;

        /// <summary>
        /// Calculates the geodesic distance between two locations, taking into account the curvature of the planet.
        /// </summary>
        /// <param name="loc1">The first location.</param>
        /// <param name="loc2">The second location.</param>
        /// <returns>
        /// The distance in kilometers.
        /// </returns>
        public static double Distance(Location loc1, Location loc2)
        {
            if(loc1 != null && loc2 != null)
            {
                double d = Math.Pow(Math.Sin(((loc1.Latitude - loc2.Latitude) * Radians) / 2), 2) + (Math.Cos(loc2.Latitude * Radians) * Math.Cos(loc1.Latitude * Radians) * Math.Pow(Math.Sin(((loc1.Longitude - loc2.Longitude) * Radians) / 2), 2));
                return Math.Atan2(Math.Sqrt(d), Math.Sqrt(1 - d)) * EarthDiameter;
            }
            return 0;
        }
    }
}
