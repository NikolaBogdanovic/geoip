namespace GeoIP
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.Contracts;
    using System.Globalization;
    using System.IO;
    using System.Runtime.Caching;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Xml;

    /// <summary>
    /// Country information.
    /// </summary>
    public sealed class Country
    {
        /// <summary>
        /// Gets the country ISO 3166-1 alpha-2 code.
        /// </summary>
        public string Alpha2Code { get; private set; }

        /// <summary>
        /// Gets the country ISO 3166-1 alpha-3 code.
        /// </summary>
        public string Alpha3Code { get; private set; }

        /// <summary>
        /// Gets the country ISO 3166-1 numeric code.
        /// </summary>
        public short? NumericCode { get; private set; }

        /// <summary>
        /// Gets the country dial code.
        /// </summary>
        public short? DialCode { get; private set; }

        /// <summary>
        /// Gets the country name.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the country description.
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        /// Gets the country olson time zone.
        /// </summary>
        public string OlsonTimeZone { get; private set; }

        /// <summary>
        /// Gets the country windows time zone.
        /// </summary>
        public string WindowsTimeZone { get; private set; }

        private Country()
        {
        }

        internal static Country Create(XmlReader reader, CancellationToken cancel)
        {
            return new Country
            {
                Alpha2Code = reader["Alpha2"],
                Alpha3Code = reader["Alpha3"],
                NumericCode = reader["Numeric"] != null ? short.Parse(reader["Numeric"], CultureInfo.InvariantCulture) : (short?)null,
                DialCode = reader["Dial"] != null ? short.Parse(reader["Dial"], CultureInfo.InvariantCulture) : (short?)null,
                Name = reader["Name"],
                Description = reader["Description"],
                OlsonTimeZone = reader["TimeZone"],
                WindowsTimeZone = TimeZones.ConvertOlsonToWindows(reader["TimeZone"], cancel)
            };
        }

        internal static async Task<Country> CreateAsync(XmlReader reader, CancellationToken cancel)
        {
            return new Country
            {
                Alpha2Code = reader["Alpha2"],
                Alpha3Code = reader["Alpha3"],
                NumericCode = reader["Numeric"] != null ? short.Parse(reader["Numeric"], CultureInfo.InvariantCulture) : (short?)null,
                DialCode = reader["Dial"] != null ? short.Parse(reader["Dial"], CultureInfo.InvariantCulture) : (short?)null,
                Name = reader["Name"],
                Description = reader["Description"],
                OlsonTimeZone = reader["TimeZone"],
                WindowsTimeZone = await TimeZones.ConvertOlsonToWindowsAsync(reader["TimeZone"], cancel).ConfigureAwait(false)
            };
        }

        internal static Country Create(int index, CancellationToken cancel)
        {
            Contract.Requires(index >= 0);

            if(--index != -1)
                return Countries.GetRaw(cancel)[index];
            return null;
        }

        internal static async Task<Country> CreateAsync(int index, CancellationToken cancel)
        {
            Contract.Requires(index >= 0);

            if(--index != -1)
                return (await Countries.GetRawAsync(cancel).ConfigureAwait(false))[index];
            return null;
        }
    }

    /// <summary>
    /// Country services.
    /// </summary>
    public static class Countries
    {
        private static readonly string DataFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Properties.Settings.Default.DataPath, "Countries.xml");
        private static readonly NameTable NameTable = new NameTable();
        private static readonly SemaphoreSlim RawLock = new SemaphoreSlim(1, 1);
        private static readonly SemaphoreSlim AllLock = new SemaphoreSlim(1, 1);

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        internal static IReadOnlyList<Country> GetRaw(CancellationToken cancel)
        {
            MemoryCache cache = MemoryCache.Default;
            var list = cache["Raw Countries"] as List<Country>;
            if(list == null)
            {
                RawLock.Wait(cancel);
                try
                {
                    list = cache.Get("Raw Countries") as List<Country>;
                    if(list == null)
                    {
                        var reader = XmlReader.Create(
                            new StringReader(Databases.ReadFile(DataFile, cancel)),
                            new XmlReaderSettings
                            {
                                DtdProcessing = DtdProcessing.Ignore,
                                IgnoreComments = true,
                                IgnoreProcessingInstructions = true,
                                IgnoreWhitespace = true,
                                NameTable = NameTable
                            });
                        if(reader.ReadToFollowing("Country"))
                        {
                            list = new List<Country>();
                            do
                            {
                                list.Add(Country.Create(reader, cancel));
                            }
                            while(reader.ReadToNextSibling("Country"));
                            cache.Add(
                                "Raw Countries",
                                list,
                                new CacheItemPolicy
                                {
                                    Priority = CacheItemPriority.NotRemovable,
                                    SlidingExpiration = TimeSpan.FromMinutes(Properties.Settings.Default.SlidingExpiration)
                                });
                        }
                    }
                }
                finally
                {
                    RawLock.Release();
                }
            }
            return list;
        }

        internal static async Task<IReadOnlyList<Country>> GetRawAsync(CancellationToken cancel)
        {
            MemoryCache cache = MemoryCache.Default;
            var list = cache["Raw Countries"] as List<Country>;
            if(list == null)
            {
                await RawLock.WaitAsync(cancel).ConfigureAwait(false);
                try
                {
                    list = cache.Get("Raw Countries") as List<Country>;
                    if(list == null)
                    {
                        var reader = XmlReader.Create(
                            new StringReader(await Databases.ReadFileAsync(DataFile, cancel).ConfigureAwait(false)),
                            new XmlReaderSettings
                            {
                                DtdProcessing = DtdProcessing.Ignore,
                                IgnoreComments = true,
                                IgnoreProcessingInstructions = true,
                                IgnoreWhitespace = true,
                                NameTable = NameTable
                            });
                        if(reader.ReadToFollowing("Country"))
                        {
                            list = new List<Country>();
                            do
                            {
                                list.Add(await Country.CreateAsync(reader, cancel).ConfigureAwait(false));
                            }
                            while(reader.ReadToNextSibling("Country"));
                            cache.Add(
                                "Raw Countries",
                                list,
                                new CacheItemPolicy
                                {
                                    Priority = CacheItemPriority.NotRemovable,
                                    SlidingExpiration = TimeSpan.FromMinutes(Properties.Settings.Default.SlidingExpiration)
                                });
                        }
                    }
                }
                finally
                {
                    RawLock.Release();
                }
            }
            return list;
        }

        /// <summary>
        /// Gets all countries list.
        /// </summary>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// All countries list.
        /// </returns>
        public static IReadOnlyList<Country> GetAll(CancellationToken cancel)
        {
            MemoryCache cache = MemoryCache.Default;
            var list = cache["All Countries"] as List<Country>;
            if(list == null)
            {
                AllLock.Wait(cancel);
                try
                {
                    list = cache.Get("All Countries") as List<Country>;
                    if(list == null)
                    {
                        list = GetRaw(cancel) as List<Country>;
                        if(list != null)
                        {
                            list = list.FindAll(c => c.Alpha3Code != null);
                            list.Sort((c1, c2) => string.CompareOrdinal(c1.Name, c2.Name));
                            cache.Add(
                                "All Countries",
                                list,
                                new CacheItemPolicy
                                {
                                    Priority = CacheItemPriority.NotRemovable,
                                    SlidingExpiration = TimeSpan.FromMinutes(Properties.Settings.Default.SlidingExpiration)
                                });
                        }
                    }
                }
                finally
                {
                    AllLock.Release();
                }
            }
            return list;
        }

        /// <summary>
        /// Gets asynchronously all countries list.
        /// </summary>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// All countries list.
        /// </returns>
        public static async Task<IReadOnlyList<Country>> GetAllAsync(CancellationToken cancel)
        {
            MemoryCache cache = MemoryCache.Default;
            var list = cache["All Countries"] as List<Country>;
            if(list == null)
            {
                await AllLock.WaitAsync(cancel).ConfigureAwait(false);
                try
                {
                    list = cache.Get("All Countries") as List<Country>;
                    if(list == null)
                    {
                        list = (await GetRawAsync(cancel).ConfigureAwait(false)) as List<Country>;
                        if(list != null)
                        {
                            list = list.FindAll(c => c.Alpha3Code != null);
                            list.Sort((c1, c2) => string.CompareOrdinal(c1.Name, c2.Name));
                            cache.Add(
                                "All Countries",
                                list,
                                new CacheItemPolicy
                                {
                                    Priority = CacheItemPriority.NotRemovable,
                                    SlidingExpiration = TimeSpan.FromMinutes(Properties.Settings.Default.SlidingExpiration)
                                });
                        }
                    }
                }
                finally
                {
                    AllLock.Release();
                }
            }
            return list;
        }
    }
}
