﻿using System;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

[assembly: CLSCompliant(true)]
[assembly: ComVisible(false)]
[assembly: Guid("b6a70614-b0b8-49dc-8e6f-0d430ff2b237")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyDefaultAlias("GeoIP Locator API")]
[assembly: AssemblyTitle("GeoIP Locator API")]
[assembly: AssemblyDescription("GeoIP Locator API")]
[assembly: AssemblyProduct("GeoIP Locator API")]
[assembly: AssemblyCompany("Nikola Bogdanović")]
[assembly: AssemblyCopyright("© 2012-2015 http://rs.linkedin.com/in/NikolaBogdanovic")]
[assembly: AssemblyTrademark("pOcHa")]
[assembly: AssemblyCulture("")]
[assembly: NeutralResourcesLanguage("en-US")]
