﻿namespace GeoIP.Properties
{
    using System.Configuration;

    /// <exclude/>
    #pragma warning disable 1591
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1604:ElementDocumentationMustHaveSummary", Justification = "Reviewed.")]
    public sealed class Settings : ApplicationSettingsBase
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes")]
        public static readonly Settings Default = (Settings)Synchronized(new Settings());
        
        [ApplicationScopedSetting]
        public string DataPath
        {
            get
            {
                return (string)this["DataPath"] ?? "App_Data";
            }
        }
        
        [ApplicationScopedSetting]
        public int SlidingExpiration
        {
            get
            {
                return this["SlidingExpiration"] as int? ?? 15;
            }
        }
    }
    #pragma warning restore 1591
}
