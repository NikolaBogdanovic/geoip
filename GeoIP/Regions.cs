namespace GeoIP
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.Contracts;
    using System.IO;
    using System.Runtime.Caching;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Xml;

    /// <summary>
    /// Region information.
    /// </summary>
    public sealed class Region
    {
        /// <summary>
        /// Gets the region country.
        /// </summary>
        public Country Country { get; private set; }

        /// <summary>
        /// Gets the region FIPS 10-4 code.
        /// </summary>
        public string FipsCode { get; private set; }

        /// <summary>
        /// Gets the region ISO 3166-2 code.
        /// </summary>
        public string IsoCode { get; private set; }

        /// <summary>
        /// Gets the region name.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the region olson time zone.
        /// </summary>
        public string OlsonTimeZone { get; private set; }

        /// <summary>
        /// Gets the region windows time zone.
        /// </summary>
        public string WindowsTimeZone { get; private set; }

        private Region()
        {
        }

        internal static Region Create(Country country, XmlReader reader, CancellationToken cancel)
        {
            return new Region
            {
                Country = country,
                FipsCode = reader["FIPS"],
                IsoCode = reader["ISO"],
                Name = reader["Name"],
                OlsonTimeZone = reader["TimeZone"],
                WindowsTimeZone = TimeZones.ConvertOlsonToWindows(reader["TimeZone"], cancel)
            };
        }

        internal static async Task<Region> CreateAsync(Country country, XmlReader reader, CancellationToken cancel)
        {
            return new Region
            {
                Country = country,
                FipsCode = reader["FIPS"],
                IsoCode = reader["ISO"],
                Name = reader["Name"],
                OlsonTimeZone = reader["TimeZone"],
                WindowsTimeZone = await TimeZones.ConvertOlsonToWindowsAsync(reader["TimeZone"], cancel).ConfigureAwait(false)
            };
        }

        internal static Region Create(int country, string region, CancellationToken cancel)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(region));

            Country c = Country.Create(country, cancel);
            if(region != "00")
            {
                foreach(Region r in Regions.GetAll(cancel))
                {
                    if(r.Country.Alpha2Code == c.Alpha2Code)
                    {
                        if(c.Alpha2Code == "US" || c.Alpha2Code == "CA")
                        {
                            if(r.IsoCode == region)
                                return r;
                        }
                        else if(r.FipsCode == region)
                            return r;
                    }
                }
            }
            return new Region
            {
                Country = c
            };
        }

        internal static async Task<Region> CreateAsync(int country, string region, CancellationToken cancel)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(region));
            
            Country c = await Country.CreateAsync(country, cancel).ConfigureAwait(false);
            if(region != "00")
            {
                foreach(Region r in await Regions.GetAllAsync(cancel).ConfigureAwait(false))
                {
                    if(r.Country.Alpha2Code == c.Alpha2Code)
                    {
                        if(c.Alpha2Code == "US" || c.Alpha2Code == "CA")
                        {
                            if(r.IsoCode == region)
                                return r;
                        }
                        else if(r.FipsCode == region)
                            return r;
                    }
                }
            }
            return new Region
            {
                Country = c
            };
        }
    }

    /// <summary>
    /// Region services.
    /// </summary>
    public static class Regions
    {
        private static readonly string DataFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Properties.Settings.Default.DataPath, "Regions.xml");
        private static readonly NameTable NameTable = new NameTable();
        private static readonly SemaphoreSlim Lock = new SemaphoreSlim(1, 1);

        /// <summary>
        /// Gets all regions list.
        /// </summary>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// All regions list.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        // ReSharper disable once ReturnTypeCanBeEnumerable.Global
        public static IReadOnlyList<Region> GetAll(CancellationToken cancel)
        {
            MemoryCache cache = MemoryCache.Default;
            var list = cache["Regions"] as List<Region>;
            if(list == null)
            {
                Lock.Wait(cancel);
                try
                {
                    list = cache.Get("Regions") as List<Region>;
                    if(list == null)
                    {
                        var reader = XmlReader.Create(
                            new StringReader(Databases.ReadFile(DataFile, cancel)),
                            new XmlReaderSettings
                            {
                                DtdProcessing = DtdProcessing.Ignore,
                                IgnoreComments = true,
                                IgnoreProcessingInstructions = true,
                                IgnoreWhitespace = true,
                                NameTable = NameTable
                            });
                        if(reader.ReadToFollowing("Country"))
                        {
                            list = new List<Region>();
                            var countries = (List<Country>)Countries.GetRaw(cancel);
                            do
                            {
                                string alpha2 = reader["Alpha2"];
                                if(reader.ReadToDescendant("Region"))
                                {
                                    Country country = countries.Find(c => c.Alpha2Code == alpha2);
                                    do
                                    {
                                        list.Add(Region.Create(country, reader, cancel));
                                    }
                                    while(reader.ReadToNextSibling("Region"));
                                }
                            }
                            while(reader.ReadToNextSibling("Country"));
                            list.Sort((r1, r2) => string.CompareOrdinal(r1.Name, r2.Name));
                            cache.Add(
                                "Regions",
                                list,
                                new CacheItemPolicy
                                {
                                    Priority = CacheItemPriority.NotRemovable,
                                    SlidingExpiration = TimeSpan.FromMinutes(Properties.Settings.Default.SlidingExpiration)
                                });
                        }
                    }
                }
                finally
                {
                    Lock.Release();
                }
            }
            return list;
        }

        /// <summary>
        /// Gets asynchronously all regions list.
        /// </summary>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// All regions list.
        /// </returns>
        public static async Task<IReadOnlyList<Region>> GetAllAsync(CancellationToken cancel)
        {
            MemoryCache cache = MemoryCache.Default;
            var list = cache["Regions"] as List<Region>;
            if(list == null)
            {
                await Lock.WaitAsync(cancel).ConfigureAwait(false);
                try
                {
                    list = cache.Get("Regions") as List<Region>;
                    if(list == null)
                    {
                        var reader = XmlReader.Create(
                            new StringReader(await Databases.ReadFileAsync(DataFile, cancel).ConfigureAwait(false)),
                            new XmlReaderSettings
                            {
                                DtdProcessing = DtdProcessing.Ignore,
                                IgnoreComments = true,
                                IgnoreProcessingInstructions = true,
                                IgnoreWhitespace = true,
                                NameTable = NameTable
                            });
                        if(reader.ReadToFollowing("Country"))
                        {
                            list = new List<Region>();
                            var countries = (List<Country>)(await Countries.GetRawAsync(cancel).ConfigureAwait(false));
                            do
                            {
                                string alpha2 = reader["Alpha2"];
                                if(reader.ReadToDescendant("Region"))
                                {
                                    Country country = countries.Find(c => c.Alpha2Code == alpha2);
                                    do
                                    {
                                        list.Add(await Region.CreateAsync(country, reader, cancel).ConfigureAwait(false));
                                    }
                                    while(reader.ReadToNextSibling("Region"));
                                }
                            }
                            while(reader.ReadToNextSibling("Country"));
                            list.Sort((r1, r2) => string.CompareOrdinal(r1.Name, r2.Name));
                            cache.Add(
                                "Regions",
                                list,
                                new CacheItemPolicy
                                {
                                    Priority = CacheItemPriority.NotRemovable,
                                    SlidingExpiration = TimeSpan.FromMinutes(Properties.Settings.Default.SlidingExpiration)
                                });
                        }
                    }
                }
                finally
                {
                    Lock.Release();
                }
            }
            return list;
        }
    }
}
