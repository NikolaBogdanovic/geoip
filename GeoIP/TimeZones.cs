﻿namespace GeoIP
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.IO;
    using System.Runtime.Caching;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Xml;

    /// <summary>
    /// Olson time zone services.
    /// </summary>
    public static class TimeZones
    {
        private static readonly string DataFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Properties.Settings.Default.DataPath, "TimeZones.xml");
        private static readonly NameTable NameTable = new NameTable();
        private static readonly SemaphoreSlim Lock = new SemaphoreSlim(1, 1);

        /// <summary>
        /// Gets all olson time zones list.
        /// </summary>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// All olson time zones list.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static NameValueCollection GetAll(CancellationToken cancel)
        {
            MemoryCache cache = MemoryCache.Default;
            var nvc = cache["Olson Time Zones"] as NameValueCollection;
            if(nvc == null)
            {
                Lock.Wait(cancel);
                try
                {
                    nvc = cache.Get("Olson Time Zones") as NameValueCollection;
                    if(nvc == null)
                    {
                        var reader = XmlReader.Create(
                            new StringReader(Databases.ReadFile(DataFile, cancel)),
                            new XmlReaderSettings
                            {
                                DtdProcessing = DtdProcessing.Ignore,
                                IgnoreComments = true,
                                IgnoreProcessingInstructions = true,
                                IgnoreWhitespace = true,
                                NameTable = NameTable
                            });
                        if(reader.ReadToFollowing("TimeZone"))
                        {
                            nvc = new NameValueCollection();
                            do
                            {
                                string key = reader["Windows"];
                                if(reader.ReadToDescendant("Olson"))
                                {
                                    do
                                    {
                                        nvc.Add(key, reader.ReadInnerXml());
                                    }
                                    while(reader.NodeType == XmlNodeType.Element);
                                }
                            }
                            while(reader.ReadToNextSibling("TimeZone"));
                            cache.Add(
                                "Olson Time Zones",
                                nvc,
                                new CacheItemPolicy
                                {
                                    Priority = CacheItemPriority.NotRemovable,
                                    SlidingExpiration = TimeSpan.FromMinutes(Properties.Settings.Default.SlidingExpiration)
                                });
                        }
                    }
                }
                finally
                {
                    Lock.Release();
                }
            }
            return nvc;
        }

        /// <summary>
        /// Gets asynchronously all olson time zones list.
        /// </summary>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// All olson time zones list.
        /// </returns>
        public static async Task<NameValueCollection> GetAllAsync(CancellationToken cancel)
        {
            MemoryCache cache = MemoryCache.Default;
            var nvc = cache["Olson Time Zones"] as NameValueCollection;
            if(nvc == null)
            {
                await Lock.WaitAsync(cancel).ConfigureAwait(false);
                try
                {
                    nvc = cache.Get("Olson Time Zones") as NameValueCollection;
                    if(nvc == null)
                    {
                        var reader = XmlReader.Create(
                            new StringReader(await Databases.ReadFileAsync(DataFile, cancel).ConfigureAwait(false)),
                            new XmlReaderSettings
                            {
                                DtdProcessing = DtdProcessing.Ignore,
                                IgnoreComments = true,
                                IgnoreProcessingInstructions = true,
                                IgnoreWhitespace = true,
                                NameTable = NameTable
                            });
                        if(reader.ReadToFollowing("TimeZone"))
                        {
                            nvc = new NameValueCollection();
                            do
                            {
                                string key = reader["Windows"];
                                if(reader.ReadToDescendant("Olson"))
                                {
                                    do
                                    {
                                        nvc.Add(key, reader.ReadInnerXml());
                                    }
                                    while(reader.NodeType == XmlNodeType.Element);
                                }
                            }
                            while(reader.ReadToNextSibling("TimeZone"));
                            cache.Add(
                                "Olson Time Zones",
                                nvc,
                                new CacheItemPolicy
                                {
                                    Priority = CacheItemPriority.NotRemovable,
                                    SlidingExpiration = TimeSpan.FromMinutes(Properties.Settings.Default.SlidingExpiration)
                                });
                        }
                    }
                }
                finally
                {
                    Lock.Release();
                }
            }
            return nvc;
        }

        /// <summary>
        /// Converts an olson time zone to a windows time zone.
        /// </summary>
        /// <param name="tz">The olson time zone.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// A windows time zone.
        /// </returns>
        public static string ConvertOlsonToWindows(string tz, CancellationToken cancel)
        {
            if(!string.IsNullOrWhiteSpace(tz))
            {
                NameValueCollection nvc = GetAll(cancel);
                for(int i = 0, j = nvc.Count; i < j; i++)
                {
                    // ReSharper disable once PossibleNullReferenceException
                    foreach(string value in nvc.GetValues(i))
                    {
                        if(value == tz)
                            return nvc.GetKey(i);
                    }
                }
                throw new ArgumentOutOfRangeException("tz", tz, @"Argument Out Of Range Exception");
            }
            return null;
        }

        /// <summary>
        /// Converts asynchronously an olson time zone to a windows time zone.
        /// </summary>
        /// <param name="tz">The olson time zone.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// A windows time zone.
        /// </returns>
        public static async Task<string> ConvertOlsonToWindowsAsync(string tz, CancellationToken cancel)
        {
            if(!string.IsNullOrWhiteSpace(tz))
            {
                NameValueCollection nvc = await GetAllAsync(cancel).ConfigureAwait(false);
                for(int i = 0, j = nvc.Count; i < j; i++)
                {
                    // ReSharper disable once PossibleNullReferenceException
                    foreach(string value in nvc.GetValues(i))
                    {
                        if(value == tz)
                            return nvc.GetKey(i);
                    }
                }
                throw new ArgumentOutOfRangeException("tz", tz, @"Argument Out Of Range Exception");
            }
            return null;
        }

        /// <summary>
        /// Converts a windows time zone to an olson time zone.
        /// </summary>
        /// <param name="id">The windows time zone.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// An olson time zone.
        /// </returns>
        public static IReadOnlyList<string> ConvertWindowsToOlson(string id, CancellationToken cancel)
        {
            if(!string.IsNullOrWhiteSpace(id))
            {
                NameValueCollection nvc = GetAll(cancel);
                for(int i = 0, j = nvc.Count; i < j; i++)
                {
                    if(nvc.GetKey(i) == id)
                        return nvc.GetValues(i);
                }
                throw new ArgumentOutOfRangeException("id", id, @"Argument Out Of Range Exception");
            }
            return null;
        }

        /// <summary>
        /// Converts asynchronously a windows time zone to an olson time zone.
        /// </summary>
        /// <param name="id">The windows time zone.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// An olson time zone.
        /// </returns>
        public static async Task<IReadOnlyList<string>> ConvertWindowsToOlsonAsync(string id, CancellationToken cancel)
        {
            if(!string.IsNullOrWhiteSpace(id))
            {
                NameValueCollection nvc = await GetAllAsync(cancel).ConfigureAwait(false);
                for(int i = 0, j = nvc.Count; i < j; i++)
                {
                    if(nvc.GetKey(i) == id)
                        return nvc.GetValues(i);
                }
                throw new ArgumentOutOfRangeException("id", id, @"Argument Out Of Range Exception");
            }
            return null;
        }
    }
}
