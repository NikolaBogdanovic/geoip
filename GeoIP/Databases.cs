namespace GeoIP
{
    using System;
    using System.ComponentModel;
    using System.Diagnostics.Contracts;
    using System.Globalization;
    using System.IO;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// Constants representing database editions.
    /// </summary>
    public enum DatabaseEdition
    {
        /// <summary>
        /// Unknown
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Countries
        /// </summary>
        Country = 1,

        /// <summary>
        /// Cities revision 1
        /// </summary>
        CityRev1 = 2,

        /// <summary>
        /// Regions revision 1
        /// </summary>
        RegionRev1 = 3,

        /// <summary>
        /// Internet Service Providers
        /// </summary>
        ISP = 4,

        /// <summary>
        /// Organizations
        /// </summary>
        Org = 5,

        /// <summary>
        /// Cities revision 0
        /// </summary>
        CityRev0 = 6,

        /// <summary>
        /// Regions revision 0
        /// </summary>
        RegionRev0 = 7,

        /// <summary>
        /// Proxies
        /// </summary>
        Proxy = 8,

        /// <summary>
        /// Autonomous System Numbers
        /// </summary>
        ASNum = 9,

        /// <summary>
        /// Network speeds revision 0
        /// </summary>
        NetSpeedRev0 = 10,

        /// <summary>
        /// Domains
        /// </summary>
        Domain = 11,

        /// <summary>
        /// Countries IPv6
        /// </summary>
        CountryV6 = 12,

        /// <summary>
        /// Autonomous System Numbers IPv6
        /// </summary>
        ASNumV6 = 21,

        /// <summary>
        /// Internet Service Providers IPv6
        /// </summary>
        ISPV6 = 22,

        /// <summary>
        /// Organizations IPv6
        /// </summary>
        OrgV6 = 23,

        /// <summary>
        /// Domains IPv6
        /// </summary>
        DomainV6 = 24,

        /// <summary>
        /// Cities IPv6 revision 1
        /// </summary>
        CityV6Rev1 = 30,

        /// <summary>
        /// Cities IPv6 revision 0
        /// </summary>
        CityV6Rev0 = 31,

        /// <summary>
        /// Network speeds revision 1
        /// </summary>
        NetSpeedRev1 = 32,

        /// <summary>
        /// Network speeds IPv6 revision 1
        /// </summary>
        NetSpeedV6Rev1 = 33
    }

    /// <summary>
    /// Database information.
    /// </summary>
    public sealed class Database
    {
        /// <summary>
        /// Gets the database information.
        /// </summary>
        public string Info { get; private set; }

        internal Database(string info)
        {
            Info = info;
        }

        /// <summary>
        /// Gets a value indicating whether this database is premium or free.
        /// </summary>
        public bool IsPremium
        {
            get
            {
                if(!string.IsNullOrWhiteSpace(Info))
                    return !Info.Contains("FREE");
                return false;
            }
        }

        /// <summary>
        /// Gets the database edition.
        /// </summary>
        public DatabaseEdition Edition
        {
            get
            {
                if(!string.IsNullOrWhiteSpace(Info))
                {
                    int db;
                    if(int.TryParse(Info.Substring(4, 3), NumberStyles.None, CultureInfo.InvariantCulture, out db))
                    {
                        if(db > 105)
                            db -= 105;
                        return (DatabaseEdition)db;
                    }
                }
                return DatabaseEdition.Unknown;
            }
        }

        /// <summary>
        /// Gets the database date.
        /// </summary>
        public DateTime Date
        {
            get
            {
                if(!string.IsNullOrWhiteSpace(Info))
                {
                    for(int i = 0, j = Info.Length; i < j - 9; i++)
                    {
                        if(!char.IsWhiteSpace(Info[i]))
                            continue;
                        DateTime dt;
                        if(DateTime.TryParseExact(Info.Substring(i + 1, 8), "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal, out dt))
                            return dt;
                        break;
                    }
                }
                return DateTime.UtcNow.Date;
            }
        }
    }

    /// <summary>
    /// Database services.
    /// </summary>
    internal static class Databases
    {
        /// <summary>
        /// Gets the hard-coded <see cref="DatabaseEdition"/> names, to avoid reflection.
        /// </summary>
        /// <param name="db">The database edition.</param>
        /// <returns>
        /// The name of a database edition.
        /// </returns>
        public static string GetName(this DatabaseEdition db)
        {
            switch(db)
            {
                case DatabaseEdition.Unknown:
                    return "Unknown";
                case DatabaseEdition.Country:
                    return "Country";
                case DatabaseEdition.CityRev0:
                case DatabaseEdition.CityRev1:
                    return "City";
                case DatabaseEdition.RegionRev0:
                case DatabaseEdition.RegionRev1:
                    return "Region";
                case DatabaseEdition.ISP:
                    return "ISP";
                case DatabaseEdition.Org:
                    return "Org";
                case DatabaseEdition.Proxy:
                    return "Proxy";
                case DatabaseEdition.ASNum:
                    return "ASNum";
                case DatabaseEdition.Domain:
                    return "Domain";
                case DatabaseEdition.CountryV6:
                    return "CountryV6";
                case DatabaseEdition.ASNumV6:
                    return "ASNumV6";
                case DatabaseEdition.ISPV6:
                    return "ISPV6";
                case DatabaseEdition.OrgV6:
                    return "OrgV6";
                case DatabaseEdition.DomainV6:
                    return "DomainV6";
                case DatabaseEdition.CityV6Rev0:
                case DatabaseEdition.CityV6Rev1:
                    return "CityV6";
                case DatabaseEdition.NetSpeedRev0:
                case DatabaseEdition.NetSpeedRev1:
                    return "NetSpeed";
                case DatabaseEdition.NetSpeedV6Rev1:
                    return "NetSpeedV6";
                default:
                    throw new InvalidEnumArgumentException("db", (int)db, typeof(DatabaseEdition));
            }
        }

        /// <summary>
        /// Gets the hard-coded <see cref="NetSpeed"/> names, to avoid reflection.
        /// </summary>
        /// <param name="ns">The network speed.</param>
        /// <returns>
        /// The name of a network speed.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public static string GetName(this NetSpeed ns)
        {
            switch(ns)
            {
                case NetSpeed.Unknown:
                    return "Unknown";
                case NetSpeed.DialUp:
                    return "Dial-Up";
                case NetSpeed.CableOrDSL:
                    return "Cable / DSL";
                case NetSpeed.Corporate:
                    return "Corporate";
                case NetSpeed.Cellular:
                    return "Cellular";
                default:
                    throw new InvalidEnumArgumentException("ns", (int)ns, typeof(NetSpeed));
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        private static FileStream GetFile(string path, bool async, CancellationToken cancel)
        {
            // ReSharper disable InconsistentNaming
            //// const long STG_E_SHAREVIOLATION = 0x80030020, STG_E_LOCKVIOLATION = 0x80030021, STIERR_SHARING_VIOLATION = 0x80070020, STIERR_LOCK_VIOLATION = 0x80070021, ERROR_SHARING_VIOLATION = 0x20, ERROR_LOCK_VIOLATION = 0x21;
            const int STG_E_SHAREVIOLATION = -2147287008, STG_E_LOCKVIOLATION = -2147287007, STIERR_SHARING_VIOLATION = -2147024864, STIERR_LOCK_VIOLATION = -2147024863, ERROR_SHARING_VIOLATION = 32, ERROR_LOCK_VIOLATION = 33;
            // ReSharper restore InconsistentNaming
            cancel.ThrowIfCancellationRequested();
            FileStream file = null;
            try
            {
                file = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read, 8192, async);
            }
            catch(Exception ex)
            {
                if(file != null)
                    file.Dispose();
                if(ex is IOException && (ex.HResult == STG_E_SHAREVIOLATION || ex.HResult == STG_E_LOCKVIOLATION || ex.HResult == STIERR_SHARING_VIOLATION || ex.HResult == STIERR_LOCK_VIOLATION || ex.HResult == ERROR_SHARING_VIOLATION || ex.HResult == ERROR_LOCK_VIOLATION))
                    return null;
                throw;
            }
            if(file.CanTimeout)
                file.ReadTimeout = file.WriteTimeout = 60000;
            return file;
        }

        internal static FileStream GetFile(string path, CancellationToken cancel)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(path));

            int delay = 10;
            while(true)
            {
                FileStream file = GetFile(path, false, cancel);
                if(file != null)
                    return file;
                cancel.ThrowIfCancellationRequested();
                Thread.Sleep(delay);
                delay *= 2;
            }
        }

        internal static async Task<FileStream> GetFileAsync(string path, CancellationToken cancel)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(path));

            int delay = 10;
            while(true)
            {
                FileStream file = GetFile(path, true, cancel);
                if(file != null)
                    return file;
                await Task.Delay(delay, cancel).ConfigureAwait(false);
                delay *= 2;
            }
        }

        internal static string ReadFile(string path, CancellationToken cancel)
        {
            using(var file = GetFile(path, cancel))
                return new StreamReader(file).ReadToEnd();
        }

        internal static async Task<string> ReadFileAsync(string path, CancellationToken cancel)
        {
            using(var file = await GetFileAsync(path, cancel).ConfigureAwait(false))
                return await new StreamReader(file).ReadToEndAsync().ConfigureAwait(false);
        }
    }
}
